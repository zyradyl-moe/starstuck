import { Category, CAT_ALT } from "../Category";
import { select_all_and_copy } from "../Copy2Clipboard";
import { setCookieBool } from "../CookieManager";

export abstract class Quirk {
    static inputField: HTMLTextAreaElement;
    static textFields: HTMLFieldSetElement;
    private category: Category;

    firstName: string;
    lastName: string;
    input: string;

    row: HTMLDivElement;
    textArea: HTMLTextAreaElement;
    activeCheckbox: HTMLInputElement;
    optionalCheckboxes: Array<HTMLInputElement>;

    constructor(firstName: string, lastName: string, category: Category = CAT_ALT, colorClass: string = firstName.toLocaleLowerCase(), lastNamePriority: boolean = false) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.category = category;
        this.optionalCheckboxes = new Array<HTMLInputElement>();

        // Create output text's elements.
        this.textArea = document.createElement("textarea");
        this.textArea.className = "textOutput";
        this.textArea.classList.add(colorClass + "Color");
        this.textArea.readOnly = true;
        this.textArea.onclick = select_all_and_copy;

        let label: HTMLLabelElement = document.createElement("label");
        if (this.lastName.length > 0) {
            label.insertAdjacentText('afterbegin', `${this.firstName} ${this.lastName}:`);
        } else {
            label.insertAdjacentText('afterbegin', `${this.firstName}:`);
        }

        this.row = document.createElement("div");
        this.row.id = firstName.toLocaleLowerCase() + "Row";
        this.row.insertAdjacentElement('beforeend', label);
        this.row.insertAdjacentElement('beforeend', this.textArea);

        Quirk.textFields.insertAdjacentElement('beforeend', this.row);

        // Create toggle checkbox.
        this.activeCheckbox = document.createElement("input");
        this.activeCheckbox.type = "checkbox";
        this.activeCheckbox.checked = true;
        this.activeCheckbox.onchange = () => this.updateVisibility();

        let tr: HTMLTableRowElement = document.createElement("tr");
        let tdTitle: HTMLTableCellElement = document.createElement("td");
        if (!lastNamePriority) {
            tdTitle.insertAdjacentText('beforeend', firstName + ":");
        } else {
            tdTitle.insertAdjacentText('beforeend', lastName + ":");
        }

        tr.insertAdjacentElement('beforeend', tdTitle)
        let tdCheckBox: HTMLTableCellElement = document.createElement("td");
        tdCheckBox.insertAdjacentElement('beforeend', this.activeCheckbox);;
        tr.onclick = () => this.activeCheckbox.click();
        tr.insertAdjacentElement('beforeend', tdCheckBox);
        document.getElementById(this.category.tabName.toLocaleLowerCase() + "Checkboxes").insertAdjacentElement('beforeend', tr);
    }

    updateVisibility(): void {
        let id = this.firstName.toLocaleLowerCase();
        let row: HTMLTableRowElement = <HTMLTableRowElement>document.getElementById(id + "Row");
        row.hidden = !this.activeCheckbox.checked;

        let optionals = <HTMLCollectionOf<HTMLInputElement>>document.getElementsByClassName(id + "Optional");
        for (let i = 0; i < optionals.length; i++) {
            optionals[i].hidden = !this.activeCheckbox.checked;
        }

        let visible = !row.hidden

        // Save setting to cookies.
        setCookieBool(this.firstName, visible, 31);

        let optionalElement: HTMLTableElement = <HTMLTableElement>document.getElementById(this.category.tabName.toLocaleLowerCase() + "Optionals");
        if (visible) {
            this.update(Quirk.inputField.value);

            if (optionalElement.hidden && optionals.length > 0) {
                optionalElement.hidden = false;
            }
        } else {
            // Check if any other optional checkboxes are visible.
            for (let i = 0; i < this.category.optionalCheckboxes.length; i++) {
                if (!this.category.optionalCheckboxes[i].hidden) {
                    return;
                }
            }

            // Hide the table.
            optionalElement.hidden = true;
        }
    }

    update(str: string): void  {
        if (!this.activeCheckbox.checked || str.length < 1) { return; }

        this.input = str;
        this.quirkify();
        this.updateTextField();
    }

    updateTextField(): void {
        this.textArea.value = this.input;

        // Auto resize.
        Quirk.autoSize(this.textArea);
    }

    // Dynamically increase the height of a textarea.
    static autoSize(element: HTMLTextAreaElement): void {
        let minHeight: number = parseInt(window.getComputedStyle(element).getPropertyValue("min-height"));

        element.style.height = "auto"; // Lets the element shrink size.
        element.style.height = `${Math.max(minHeight, element.scrollHeight)}px`;
    }

    addCheckbox(label: string, title: string, defaultValue: boolean = false): HTMLInputElement {
        let checkbox = document.createElement("input");
        checkbox.type = "checkbox";
        checkbox.id = label;
        checkbox.checked = defaultValue;
        checkbox.onchange = () => {
            setCookieBool(this.firstName + checkbox.id, checkbox.checked, 31);
            this.update(Quirk.inputField.value);
        }

        let tr: HTMLTableRowElement = document.createElement("tr");
        tr.onclick = () => checkbox.click();
        let tdTitle: HTMLTableCellElement = document.createElement("td");
        tdTitle.insertAdjacentText('beforeend', this.firstName + " ~ " + label + ":");

        tr.insertAdjacentElement('beforeend', tdTitle);
        tr.title = title;
        tr.className = this.firstName.toLocaleLowerCase() + "Optional";
        let tdCheckBox: HTMLTableCellElement = document.createElement("td");
        tdCheckBox.insertAdjacentElement('beforeend', checkbox);
        tr.insertAdjacentElement('beforeend', tdCheckBox);
        document.getElementById(this.category.tabName.toLocaleLowerCase() + "Optionals").insertAdjacentElement('beforeend', tr);

        this.category.optionalCheckboxes.push(tr);
        this.optionalCheckboxes.push(checkbox);
        return checkbox;
    }

    abstract quirkify(): void;

    lowerCase(): void {
        this.input = this.input.toLocaleLowerCase();
    }

    upperCase(): void {
        this.input = this.input.toLocaleUpperCase();
    }

    prefix(str: string): void {
        this.input = str + this.input;
    }

    suffix(str: string): void {
        this.input += str;
    }

    replaceStr(pattern: string, replace: string, matchCase: boolean = false, caseinsensitive: boolean = false): void {
        let reg: RegExp;
        if (!matchCase) {
            if (!caseinsensitive) {
                reg = new RegExp(pattern, "g");
            } else {
                reg = new RegExp(pattern, "gi");
            }
            this.input = this.input.replace(reg, replace);
        } else {
            reg = new RegExp(pattern, "gi");
            this.input = this.input.replace(reg, function(match) {
                return Quirk.matchCase(replace, match);
            });
        }
    }

    replaceWord(pattern: string, replace: string, matchCase: boolean = false, caseinsensitive: boolean = false): void {
        this.replaceStr("\\b" + pattern + "\\b", replace, matchCase, caseinsensitive);
    }

    // Function graciously stolen from https://stackoverflow.com/a/17265031/6446221.
    static matchCase(text: string, pattern: string): string {
        // If the entire text is uppercase then uppercase the whole pattern regardless of lengths.
        if (pattern.toUpperCase() === pattern) {
            return text.toUpperCase();
        }

        var result = '';

        for (var i = 0; i < text.length; i++) {
            var c = text.charAt(i);
            var p = pattern.charCodeAt(i);

            if (p >= 65 && p < 65 + 26) {
                result += c.toUpperCase();
            } else {
                result += c.toLowerCase();
            }
        }

        return result;
    }

    changeCase(pattern: string, upper: boolean): void {
        let reg: RegExp = new RegExp(pattern, "g");
        this.input = this.input.replace(reg, function(match) {
            if (upper) {
                return match.toLocaleUpperCase();
            }
            return match.toLocaleLowerCase();
        });
    }

    randReplace(pattern: string, replace: string, prob: number): void {
        let reg: RegExp = new RegExp(pattern, "g");
        this.input = this.input.replace(reg, function(match) {
            if (Math.random() <= prob) {
                // A little hack for capture groups.
                return replace.replace("$1", match);
            }
            return match;
        });
    }

    // Troll-specific stuff below.

    // $1 capture group for eyes.
    // $2 capture group for mouth.
    replaceEmotes(replace: string): void {
        let eyes = "[:;]";
        let mouth = "[\\)\\(Dd]";
        this.changeCase(`(${eyes})(${mouth})`, true);

        let reg: RegExp = new RegExp(`(${eyes})(${mouth})`, "gi");
        this.input = this.input.replace(reg, replace);
    }

    trollifyCustom(): void {
        this.replaceWord("Best Buy", "Acceptable Purchase", true);
        this.replaceWord("Geek Squad", "Thinkpan Platoon", true);
        this.replaceWord("coffee", "bean extract", true);
        this.replaceWord("food court", "meal arena", true);
        this.replaceWord("Genius Bar", "intellectual counter", true);
    }

    trollifyCanon(): void {
        this.replaceWord("christmas", "12th perigee\'s eve", true);
        this.replaceWord("christmas eve", "12th perigee\'s eve", true);
        this.replaceWord("guts", "abdominal sausages", true);
        this.replaceWord("bathrobe", "ablutionrobe", true);
        this.replaceWord("bath robe", "ablutionrobe", true);
        this.replaceWord("bathtub", "ablution trap", true);
        this.replaceWord("bath tub", "ablution trap", true);
        this.replaceWord("stomach", "acid trap", true);
        this.replaceStr("intestine", "acid tube", true);
        this.replaceWord("rap", "alternian slam poetry", true);
        this.replaceWord("sheep", "baabeast", true);
        this.replaceWord("lamb", "baby baabeast", true);
        this.replaceStr("brief", "banana hammock", true);
        this.replaceStr("dog", "barkbeast", true);
        this.replaceWord("puppy", "baby barkbeast", true);
        this.replaceWord("puppies", "baby barkbeasts", true);
        this.replaceWord("cattle", "beefgrubs", true);
        this.replaceStr("lung", "bellowsac", true);
        this.replaceWord("ribs", "bellowsac enclosures", true);
        this.replaceWord("coleslaw", "bileslaw", true);
        this.replaceStr("oyster", "bivalvular sea critter", true);
        this.replaceWord("room", "block", true);
        this.replaceWord("water under the bridge", "blood beneath the abbatoir", true);
        this.replaceWord("vocal sac", "blow sack", true);
        this.replaceStr("bobblehead", "bobblenug", true);
        this.replaceWord("maple syrup", "boiled tree blood", true);
        this.replaceWord("penis", "bone bulge", true);
        this.replaceWord("dick", "bulge", true);
        this.replaceStr("cock", "bulge", true);
        this.replaceWord("pussy", "nook", true);
        this.replaceWord("vagina", "bone nook", true);
        this.replaceWord("cunt", "nook", true);
        this.replaceWord("library", "bookhive", true);
        this.replaceWord("fan", "breeze blender", true);
        this.replaceWord("bra", "globehugger", true);
        this.replaceWord("popcorn", "buttery exploded kernels", true);
        this.replaceWord("nose", "cartilaginous nub", true);
        this.replaceWord("last name", "caste name", true);
        this.replaceWord("rectum", "chagrin tunnel", true);
        this.replaceWord("mouse", "cheese critter", true);
        this.replaceWord("mice", "cheese critters", true);
        this.replaceStr("cheekbone", "cheekblade", true);
        this.replaceStr("cheek bone", "cheekblade", true);
        this.replaceWord("larnyx", "chitinous windhole", true);
        this.replaceWord("neck", "chug collumn", true);
        this.replaceWord("butter", "churned dairy product", true);
        this.replaceStr("chicken", "cluckbeast", true);
        this.replaceWord("chicken egg", "cluckbeast ova", true);
        this.replaceWord("chicken eggs", "cluckbeast ovi", true);
        this.replaceWord("hit-and-run", "collide-and-scamper", true);
        this.replaceWord("hit and run", "collide-and-scamper", true);
        this.replaceWord("apartment complex", "communal hive stem", true);
        this.replaceWord("apartment tower", "communal hive stem", true);
        this.replaceWord("apartment", "communal hive", true);
        this.replaceWord("cup of tea", "container of scalding leaf fluid", true);
        this.replaceWord("casket", "corpse box", true);
        this.replaceWord("coffin", "corpse box", true);
        this.replaceWord("stove", "crisprange", true);
        this.replaceWord("oven", "crisprange", true);
        this.replaceWord("kool aid", "cruel aid", true);
        this.replaceStr("worm", "dirt noodle", true);
        this.replaceStr("earthworm", "dirt noodle", true);
        this.replaceWord("herring", "distraction fish", true);
        this.replaceWord("tears", "dismay fluid", true);
        this.replaceStr("bell", "dong shouter", true);
        this.replaceStr("dumpster", "dross coffer", true);
        this.replaceStr("trash can", "dross coffer", true);
        this.replaceStr("wastebasket", "dross coffer", true);
        this.replaceStr("garbage can", "dross coffer", true);
        this.replaceWord("japan", "eastern alternia", true);
        this.replaceWord("hentai", "eastern alternian pornography", true);
        this.replaceWord("anime", "eastern alternian animation", true);
        this.replaceWord("quotation marks", "enclosure talons", true);
        this.replaceWord("backbone", "exoskeleton", true);
        this.replaceWord("balcony", "gander precipice", true);
        this.replaceStr("skateboard", "falldown slat", true);
        this.replaceWord("skateboarding", "falldown slatting", true);
        this.replaceWord("imaginary friend", "fantasy phantom", true);
        // this.replaceWord("beans", "fart niblets", true);
        this.replaceStr("period", "finish crumb", true);
        this.replaceStr("pizza", "flavor disc", true);
        this.replaceWord("valentine\'s day", "flushed affirmation day", true);
        this.replaceWord("leg", "frond", true);
        this.replaceWord("knee", "frond hinge", true);
        this.replaceWord("tree", "frond nub", true);
        this.replaceStr("eyeball", "gander bulb", true);
        this.replaceWord("ass", "gastric evacuation gland", true);
        this.replaceStr("eye", "glance nugget", true);
        this.replaceWord("turkey", "gobble fiend", true);
        this.replaceWord("turkies", "gobble fiends", true);
        this.replaceWord("google", "goregle", true);
        this.replaceWord("ham-fisted", "grub-fisted", true);
        this.replaceStr("pancake", "grubcake", true);
        this.replaceWord("ice cream", "grubcream", true);
        this.replaceStr("omlette", "grublette", true);
        this.replaceWord("meatloaf", "grubloaf", true);
        this.replaceWord("meat and potatos", "grubloaf and tuber paste", true);
        this.replaceStr("toolshed", "grubshed", true);
        this.replaceStr("woodshed", "grubshed", true);
        this.replaceStr("barn", "grubshed", true);
        this.replaceWord("youtube", "grubtube", true);
        this.replaceWord("bratwurst", "grubwurst", true);
        this.replaceWord("born", "hatched", true);
        this.replaceStr("ear canal", "hear duct", true);
        this.replaceStr("home", "hive", true);
        this.replaceStr("railing", "hold pole", true);
        this.replaceWord("goose", "honkbird", true);
        this.replaceWord("geese", "honkbirds", true);
        this.replaceStr("horse", "hoofbeast", true);
        this.replaceWord("bullshit", "hoofbeast manure", true);
        this.replaceWord("horse shit", "hoofbeast manure", true);
        this.replaceWord("refridgerator", "hunger trunk", true);
        this.replaceWord("fridge", "hunger trunk", true);
        this.replaceWord("pie", "huskloaf", true);
        this.replaceStr("laptop", "husktop", true);
        this.replaceStr("soda can", "hydration cylinder", true);
        this.replaceStr("pop can", "hydration cylinder", true);
        this.replaceWord("bro", "invertebrother", true);
        this.replaceWord("sis", "invertesister", true);
        this.replaceWord("picnic", "lawnmeal", true);
        this.replaceWord("yard", "lawnring", true);
        this.replaceWord("toilet", "load gaper", true);
        this.replaceWord("brainstem", "lobestem", true);
        this.replaceWord("sofa", "loungeplank", true);
        this.replaceWord("couch", "loungeplank", true);
        this.replaceWord("ant", "marchbug", true);
        this.replaceWord("ants", "marchbugs", true);
        this.replaceWord("kitchen", "meal block", true);
        this.replaceStr("throat", "meal tunnel", true);
        this.replaceStr("freezer", "meal vault", true);
        this.replaceStr("cow", "moobeast", true);
        this.replaceWord("centaur", "musclebeast", true);
        this.replaceWord("icing on the cake", "mucus on the grubloaf", true);
        this.replaceWord("cherry on the cake", "mucus on the grubloaf", true);
        this.replaceWord("rat", "nibble vermin", true);
        this.replaceWord("foot", "balance nub", true);
        this.replaceWord("feet", "balance nubs", true);
        this.replaceStr("head", "nugbone", true);
        this.replaceStr("squirrel", "nut creature", true);
        this.replaceWord("bus", "omniscuttlecoach", true);
        this.replaceStr("unicycle", "one wheel device", true);
        this.replaceWord("cellphone", "palmhusk", true);
        this.replaceWord("phone", "palmhusk", true);
        this.replaceWord("spine", "posture pole", true);
        this.replaceWord("jugular", "primary spurt artery", true);
        this.replaceWord("finger", "prong", true);
        this.replaceWord("esophagus", "protein chute", true);
        this.replaceWord("heart", "pump biscuit", true);
        this.replaceWord("child", "pupa", true);
        this.replaceWord("vampire", "rainbow drinker", true);
        this.replaceWord("bed", "recuperacoon", true);
        this.replaceWord("bedroom", "respiteblock", true);
        this.replaceWord("breasts", "rumble spheres", true);
        this.replaceWord("tea", "scalding leaf fluid", true);
        this.replaceWord("house", "hive", true);
        this.replaceWord("car", "scuttlebuggy", true);
        this.replaceWord("testicles", "shame globes", true);
        this.replaceWord("pool", "slither basin", true);
        this.replaceWord("blanket", "snuggleplane", true);
        this.replaceWord("year", "sweep", true);
        this.replaceWord("city", "stemcluster", true);
        this.replaceWord("brain", "think pan", true);
        this.replaceWord("bookworm", "tomewriggler", true);
        this.replaceWord("elephant", "trunkbeast", true);
        this.replaceWord("mashed potatos", "tuber paste", true);
        this.replaceWord("potato", "tuber", true);
        this.replaceWord("potatos", "tubers", true);
        this.replaceWord("bicycle", "two wheel device", true);
        this.replaceWord("month", "wipe", true);
        this.replaceWord("birthday", "wriggling day", true);
        this.replaceWord("stairs", "zigzag incline", true);
    }

    catPuns(): void {
        this.replaceStr("mother", "meowther", true);
        this.replaceStr("for", "fur", true);
        this.replaceStr("pause", "paws", true);
        this.replaceStr("cause", "claws", true);
        this.replaceStr("now", "meow", true);
        this.replaceStr("(per|pre)", "pur", true);
    }

    fishPuns(): void {
        this.replaceStr("friend", "frond", true);
        this.replaceStr("fond", "frond", true);
        this.replaceStr("each", "beach", true);
        this.replaceStr("attitude", "attitide", true);
        this.replaceStr("tidy", "tidey", true);
        this.replaceStr("totally", "tidally", true);
        this.replaceStr("sanitary", "sanditary", true);
        this.replaceStr("gullible", "seagullible", true);
        this.replaceStr("jealous", "jellyfish", true);
        this.replaceStr("sooner", "schooner", true);
        this.replaceStr("actually", "actshoally", true);
        this.replaceStr("surgeon", "sturgeon", true);
        this.replaceStr("not", "naut", true);
        this.replaceStr("naughty", "nauty", true);
        this.replaceStr("accquianted", "aquainted", true);
        this.replaceStr("serious", "searious", true);
        this.replaceStr("secret", "seacret", true);
        this.replaceStr("tune", "tuna", true);
        this.replaceStr("yourself", "yourshellf", true);
        this.replaceStr("myself", "myshellf", true);
        this.replaceStr("simple", "shrimple", true);
        this.replaceStr("simply", "shrimply", true);
        this.replaceStr("enemy", "anemone", true);
        this.replaceStr("any", "anemone", true);
        this.replaceStr("inaccurate", "finaccurate", true);
        this.replaceStr("important", "finportant", true);
        this.replaceStr("girl", "gill", true);
        this.replaceStr("flipping", "flippering", true);
        this.replaceStr("specific", "pacific", true);
        this.replaceStr("boy", "buoy", true);
        this.replaceStr("turn", "tern", true);
        this.replaceStr("puffing", "puffin", true);
        this.replaceStr("lunacy", "tunasea", true);
        this.replaceStr("otherwise", "otterwise", true);
        this.replaceStr("haul", "hull", true);
        this.replaceStr("god", "cod", true);
        this.replaceStr("could", "cod", true);
        this.replaceStr("pawn", "prawn", true);
        this.replaceStr("sardonic", "sardinonic", true);
        this.replaceStr("hero", "heroe", true);
        this.replaceStr("heroine", "heroeine", true);
        this.replaceWord("row", "roe", true);
        this.replaceStr("routine", "roetine", true);
        this.replaceStr("opportunity", "opportunaty", true);
        this.replaceStr("purchase", "perchase", true);
        this.replaceStr("hilarious", "gillarious", true);
        this.replaceStr("illegal", "gillegal", true);
        this.replaceStr("had", "haddock", true);
        this.replaceStr("outrageous", "outraygeous", true);
        this.replaceStr("cuddle", "cuttle", true);
        this.replaceStr("hullabaloo", "hullabeluga", true);
        this.replaceStr("pick", "pike", true);
        this.replaceStr("like", "pike", true);
        this.replaceStr("purpose", "porpoise", true);
        this.replaceStr("prepose", "porpoise", true);
        this.replaceStr("propose", "porpoise", true);
        this.replaceStr("brilliant", "krilliant", true);
        this.replaceStr("crappy", "crappie", true);
        this.replaceWord("bet", "betta", true);
        this.replaceStr("better", "betta", true);
        this.replaceStr("fantastic", "fintastic", true);
        this.replaceStr("fantasy", "fintasy", true);
        this.replaceStr("believe", "bereef", true);
        this.replaceStr("practicing", "pracfishing", true);
        this.replaceWord("elated", "eelated", true);
        this.replaceStr("amscray", "clamscray", true);
        this.replaceStr("moirail", "morayrail", true);
        this.replaceStr("kismesis", "fishmesis", true);
        this.replaceStr("auspistice", "ausfishtice", true);
        this.replaceStr("snap", "snapper", true);
        this.replaceStr("special", "speshell", true);
        this.replaceStr("always", "alwaves", true);
        this.replaceStr("fumbling", "floundering", true);
        this.replaceStr("observations", "lobstervations", true);
        this.replaceStr("muscle", "mussel", true);
        this.replaceStr("wailing", "whaling", true);
        this.replaceStr("help", "kelp", true);
        this.replaceStr("girl", "gill", true);
        this.replaceStr("kill", "krill", true);
        this.replaceStr("well", "whale", true);
        this.replaceStr("welcome", "whalecome", true);
        this.replaceStr("debate", "debait", true);
        this.replaceStr("doofus", "doofish", true);
        this.replaceStr("creative", "crayative", true);
        this.replaceStr("creature", "crayture", true);
        this.replaceStr("hell", "shell", true);
        this.replaceStr("sell", "shell", true);
        this.replaceStr("really", "reely", true);
        this.replaceStr("coy", "koi", true);
        this.replaceStr("hearing", "herring", true);
        this.replaceStr("selfish", "shellfish", true);
        this.replaceStr("something", "somefin", true);
        this.replaceStr("nothing", "nofin", true);
        this.replaceStr("feeling", "eeling", true);
        this.replaceStr("fine", "fin", true);
        this.replaceStr("see", "sea", true);
        this.replaceStr("should", "shoald", true);
        this.replaceStr("about", "aboat", true);
        this.replaceStr("kid", "squid", true);
        this.replaceStr("sure", "shore", true);
        this.replaceWord("crap", "carp", true);
        this.replaceWord("(what are|what do)", "water", true);
    }

    tiaraEmotes(): void {
        this.replaceEmotes("38$2");
    }

    quadhornsEmotes(): void {
        this.replaceEmotes("ꓘ8$2");
    }

    clownEmotes(): void {
        this.replaceEmotes(":O$2");
    }

    censorSwears(extreme: boolean = false): void {
        this.replaceWord("fuck", "f*ck", true);
        this.replaceWord("bitch", "b*tch", true);
        this.replaceWord("nigger", "n*gger", true);
        this.replaceWord("niggers", "n*ggers", true);
        this.replaceWord("shit", "sh*t", true);
        this.replaceWord("damn", "d*mn", true);
        this.replaceWord("crap", "cr*p", true);

        if (extreme) {
            this.replaceStr("whoops", "wh**ps", true);
            this.replaceStr("silly", "s*lly"), true;
            this.replaceStr("shoot", "sh**t", true);
            this.replaceStr("fidging", "f*dging", true);
        }
    }

    alternatingCaps(): void {
        let result: string = "";
        let cap: boolean = true;

        for (let i = 0; i < this.input.length; i++) {
            let c = this.input.charAt(i);
            
            if (c.match(/\w/i)) {
                result += cap ? c.toLocaleUpperCase() : c.toLocaleLowerCase();
                cap = !cap;
            } else {
                result += c;
            }
        }
        this.input = result;
    }

    parabolaCase(): void {
        let str: Array<String> = this.input.toLowerCase().split(' ');

        for (let i = 0; i < str.length; i++) {
            let word: Array<String> = str[i].split('');

            for (let e = 1; e < word.length; e++) {
                let cur: number = word.length - e;
                if (word[cur].match(/[a-z]/)) {
                    word[cur] = word[cur].toUpperCase();
                    break;
                }
            }
            word[0] = word[0].toUpperCase();
            str[i] = word.join('');
        }
        this.input = str.join(' ');
    }

    titleCase(): void {
        this.input = this.input.toLowerCase().split(' ').map(function(word) {
            return (word.charAt(0).toUpperCase() + word.slice(1));
        }).join(' ');
    }
    
}
