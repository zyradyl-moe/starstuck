import { Quirk } from "./Quirk";
import { list } from "../Category";


import { Sarina } from "./Starstuck/Sarina";
import { Keirno } from "./Starstuck/Keirno";
import { Yheana } from "./Starstuck/Yheana";
import { Sacoph } from "./Starstuck/Sacoph";
import { Ekkiyo } from "./Starstuck/Ekkiyo";
import { Barist } from "./Starstuck/Barist";
import { Pulera } from "./Starstuck/Pulera";

import { Aradia } from "./Alternia/Aradia";
import { Equius } from "./Alternia/Equius";
import { Eridan } from "./Alternia/Eridan";
import { Feferi } from "./Alternia/Feferi";
import { Gamzee } from "./Alternia/Gamzee";
import { Kanaya } from "./Alternia/Kanaya";
import { Karkat } from "./Alternia/Karkat";
import { Nepeta } from "./Alternia/Nepeta";
import { Sollux } from "./Alternia/Sollux";
import { Tavros } from "./Alternia/Tavros";
import { Terezi } from "./Alternia/Terezi";
import { Vriska } from "./Alternia/Vriska";

import { Aranea } from "./Beforus/Aranea";
import { Cronus } from "./Beforus/Cronus";
import { Horuss } from "./Beforus/Horuss";
import { Kankri } from "./Beforus/Kankri";
import { Kurloz } from "./Beforus/Kurloz";
import { Latula } from "./Beforus/Latula";
import { Meenah } from "./Beforus/Meenah";
import { Meulin } from "./Beforus/Meulin";
import { Mituna } from "./Beforus/Mituna";
import { Porrim } from "./Beforus/Porrim";
import { Rufioh } from "./Beforus/Rufioh";

import { Caliborn } from "./Cherubs/Caliborn";
import { Calliope } from "./Cherubs/Calliope";

import { Tavrisprite } from "./Sprites/Tavrisprite";
import { Erisolsprite } from "./Sprites/Erisolsprite";

import { Xefros } from "./Hiveswap/Xefros";
import { Trizza } from "./Hiveswap/Trizza";
import { Diemen } from "./Hiveswap/Diemen";
import { Ardata } from "./Hiveswap/Ardata";
import { Amisia } from "./Hiveswap/Amisia";
import { Skylla } from "./Hiveswap/Skylla";
import { Bronya } from "./Hiveswap/Bronya";
import { Tagora } from "./Hiveswap/Tagora";
import { Vikare } from "./Hiveswap/Vikare";
import { Polypa } from "./Hiveswap/Polypa";
import { Zebruh } from "./Hiveswap/Zebruh";
import { Elwurd } from "./Hiveswap/Elwurd";
import { Kuprum } from "./Hiveswap/Kuprum";
import { Folykl } from "./Hiveswap/Folykl";
import { Remele } from "./Hiveswap/Remele";
import { Tyzias } from "./Hiveswap/Tyzias";
import { Chixie } from "./Hiveswap/Chixie";
import { Azdaja } from "./Hiveswap/Azdaja";
import { Chahut } from "./Hiveswap/Chahut";
import { Zebede } from "./Hiveswap/Zebede";
import { Tegiri } from "./Hiveswap/Tegiri";
import { Mallek } from "./Hiveswap/Mallek";
import { Lynera } from "./Hiveswap/Lynera";
import { Tirona } from "./Hiveswap/Tirona";
import { Boldir } from "./Hiveswap/Boldir";
import { Stelsa } from "./Hiveswap/Stelsa";
import { Marsti } from "./Hiveswap/Marsti";
import { Karako } from "./Hiveswap/Karako";
import { Wanshi } from "./Hiveswap/Wanshi";
import { Fozzer } from "./Hiveswap/Fozzer";
import { Daraya } from "./Hiveswap/Daraya";
import { Nihkee } from "./Hiveswap/Nihkee";
import { Lanque } from "./Hiveswap/Lanque";
import { Barzum } from "./Hiveswap/Barzum";
import { Baisil } from "./Hiveswap/Baizil";

export function loadQuirkElements(): void {
    Quirk.inputField = <HTMLTextAreaElement>document.getElementById("textInput");
    Quirk.inputField.oninput = updateText;
    Quirk.textFields = <HTMLFieldSetElement>document.getElementById("textFields");


    list[0].addQuirk(new Sarina());
    list[0].addQuirk(new Barist());
    list[0].addQuirk(new Pulera());
    list[0].addQuirk(new Keirno());
    list[0].addQuirk(new Ekkiyo());
    list[0].addQuirk(new Yheana());
    list[0].addQuirk(new Sacoph());

    list[1].addQuirk(new Aradia());
    list[1].addQuirk(new Tavros());
    list[1].addQuirk(new Sollux());
    list[1].addQuirk(new Karkat());
    list[1].addQuirk(new Nepeta());
    list[1].addQuirk(new Kanaya());
    list[1].addQuirk(new Terezi());
    list[1].addQuirk(new Vriska());
    list[1].addQuirk(new Equius());
    list[1].addQuirk(new Gamzee());
    list[1].addQuirk(new Eridan());
    list[1].addQuirk(new Feferi());

    list[2].addQuirk(new Rufioh());
    list[2].addQuirk(new Mituna());
    list[2].addQuirk(new Kankri());
    list[2].addQuirk(new Meulin());
    list[2].addQuirk(new Porrim());
    list[2].addQuirk(new Latula());
    list[2].addQuirk(new Aranea());
    list[2].addQuirk(new Horuss());
    list[2].addQuirk(new Kurloz());
    list[2].addQuirk(new Cronus());
    list[2].addQuirk(new Meenah());

    list[3].addQuirk(new Caliborn());
    list[3].addQuirk(new Calliope());

    list[4].addQuirk(new Tavrisprite());
    list[4].addQuirk(new Erisolsprite());

    list[5].addQuirk(new Xefros());
    list[5].addQuirk(new Trizza());
    list[5].addQuirk(new Diemen());
    list[5].addQuirk(new Ardata());
    list[5].addQuirk(new Amisia());
    list[5].addQuirk(new Skylla());
    list[5].addQuirk(new Bronya());
    list[5].addQuirk(new Tagora());
    list[5].addQuirk(new Vikare());
    list[5].addQuirk(new Polypa());
    list[5].addQuirk(new Zebruh());
    list[5].addQuirk(new Elwurd());
    list[5].addQuirk(new Kuprum());
    list[5].addQuirk(new Folykl());
    list[5].addQuirk(new Remele());
    list[5].addQuirk(new Tyzias());
    list[5].addQuirk(new Chixie());
    list[5].addQuirk(new Azdaja());
    list[5].addQuirk(new Chahut());
    list[5].addQuirk(new Zebede());
    list[5].addQuirk(new Tegiri());
    list[5].addQuirk(new Mallek());
    list[5].addQuirk(new Lynera());
    list[5].addQuirk(new Tirona());
    list[5].addQuirk(new Boldir());
    list[5].addQuirk(new Stelsa());
    list[5].addQuirk(new Marsti());
    list[5].addQuirk(new Karako());
    list[5].addQuirk(new Wanshi());
    list[5].addQuirk(new Fozzer());
    list[5].addQuirk(new Daraya());
    list[5].addQuirk(new Nihkee());
    list[5].addQuirk(new Lanque());
    list[5].addQuirk(new Barzum());
    list[5].addQuirk(new Baisil());

    // Make optional checkboxes table visible from the start if there are entries.
    for (let i = 0; i < list.length; i++) {
        if (list[i].optionalCheckboxes.length > 0) {
            let id = list[i].tabName.toLocaleLowerCase() + "Optionals";
            document.getElementById(id).hidden = false;
        }
    }

    // Add in debug text.
    let str: string = "The Apple Is Loose!!! :D";
    Quirk.inputField.value = str;
    Quirk.inputField.dispatchEvent(new Event("input"));
    // Remove the debug text when the element's selected for the first time.
    Quirk.inputField.addEventListener('focus', function () {
        if (removeStart) {
            Quirk.inputField.value = "";
            Quirk.inputField.dispatchEvent(new Event("input"));
            removeStart = false;
        }
    });
}
let removeStart = true;

function updateText(event: MouseEvent): void {
    let input = <HTMLTextAreaElement>event.currentTarget;
    let inputStr: string = input.value;
    Quirk.autoSize(input);

    // Wipe all inputs if empty. (stops deleted text from not updating the outputs)
    if (inputStr.length < 1) {
        let txts = <HTMLCollectionOf<HTMLTextAreaElement>>document.getElementsByClassName("textOutput");
        for (let i = 0; i < txts.length; i++) {
            txts[i].value = "";
            Quirk.autoSize(txts[i]);
        }

        return;
    }

    for (let i = 0; i < list.length; i++) {
        for (let j = 0; j < list[i].quirks.length; j++) {
            list[i].quirks[j].update(inputStr);
        }
    }
}
