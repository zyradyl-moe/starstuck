import { Quirk } from "../Quirk";
import { CAT_STA } from "../../Category";

export class Pulera extends Quirk {

    constructor() {
        super("Pulera", "??????", CAT_STA, "nepeta");
    }

    quirkify(): void {
        if ((<HTMLInputElement>document.getElementById("chkToggleCT")).checked) {
            this.trollifyCustom();
        }
        
        if ((<HTMLInputElement>document.getElementById("chkToggleTC")).checked) {
            this.trollifyCanon();
        }

        this.lowerCase();

        this.replaceStr("i", "iii");
        this.replaceStr("a", "aaa");
    }
}