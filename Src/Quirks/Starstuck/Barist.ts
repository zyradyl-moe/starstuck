import { Quirk } from "../Quirk";
import { CAT_STA } from "../../Category";

export class Barist extends Quirk {

    constructor() {
        super("Barista", "?????", CAT_STA, "kanaya");
    }

    quirkify(): void {
        if ((<HTMLInputElement>document.getElementById("chkToggleCT")).checked) {
            this.trollifyCustom();
        }
        
        if ((<HTMLInputElement>document.getElementById("chkToggleTC")).checked) {
            this.trollifyCanon();
        }

        this.parabolaCase();
    }
    
}
