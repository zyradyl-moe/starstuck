import { Quirk } from "../Quirk";
import { CAT_STA } from "../../Category";

export class Yheana extends Quirk {
    calm: HTMLInputElement;
    anger: HTMLInputElement;
    rage: HTMLInputElement;
    trollian: HTMLInputElement;
    encrypted: HTMLInputElement;

    constructor() {
        super("Yheana", "Liuhir", CAT_STA, "gamzee");
        this.calm = this.addCheckbox("Calm Mood", "Yheana is in a calm mood", true);
        this.anger = this.addCheckbox("Angry", "Yheana is becoming angrier", false);
        this.rage = this.addCheckbox("Enraged", "Yheana has lost all of her shit", false);
        this.trollian = this.addCheckbox("Trollian Chat Handle", "Prefix with trollian chat handle", false);
        this.encrypted = this.addCheckbox("Encrypted Trollian Chat", "Add [OTP] prefix", false);
    }

    quirkify(): void {
        if ((<HTMLInputElement>document.getElementById("chkToggleCT")).checked) {
            this.trollifyCustom();
        }
        
        if ((<HTMLInputElement>document.getElementById("chkToggleTC")).checked) {
            this.trollifyCanon();
        }

        if (this.calm.checked) {
            this.replaceStr("ho", "HOOHOOHOO");
            this.replaceStr("he", "HEEHEEHEE");
            this.replaceStr("ha", "HAAHAAHAA");
            this.lowerCase();
        }

        if (this.anger.checked) {
            this.replaceStr("ho", "HOOHOOHOO");
            this.replaceStr("he", "HEEHEEHEE");
            this.replaceStr("ha", "HAAHAAHAA");
        }

        if (this.rage.checked) {
            this.replaceStr("ho", "HOOHOOHOO");
            this.replaceStr("he", "HEEHEEHEE");
            this.replaceStr("ha", "HAAHAAHAA");
            this.upperCase();
        }

        this.clownEmotes();

        if (this.trollian.checked) {
            this.prefix("gigglingTheologian: ");
            if (this.encrypted.checked) {
                this.prefix("[OTP]");
            }
            this.prefix("```\n"); 
            this.suffix("\n```");
        }
    }
}