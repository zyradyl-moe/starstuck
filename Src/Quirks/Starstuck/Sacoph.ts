import { Quirk } from "../Quirk";
import { CAT_STA } from "../../Category";

export class Sacoph extends Quirk {
    puns: HTMLInputElement;
    trollian: HTMLInputElement;
    encrypted: HTMLInputElement;

    constructor() {
        super("Sacoph", "Yoyahu", CAT_STA, "feferi");
        this.puns = this.addCheckbox("Fish Puns", "Shellf-explanatory!", true);
        this.trollian = this.addCheckbox("Trollian Chat Handle", "Prefix with trollian chat handle", false);
        this.encrypted = this.addCheckbox("Encrypted Trollian Chat", "Add [OTP] prefix", false);
    }

    quirkify(): void {
        if ((<HTMLInputElement>document.getElementById("chkToggleCT")).checked) {
            this.trollifyCustom();
        }
        
        if ((<HTMLInputElement>document.getElementById("chkToggleTC")).checked) {
            this.trollifyCanon();
        }
        
        if (this.puns.checked) {
            this.fishPuns();
        }

        this.replaceStr("[Aa]", "★");
        this.prefix("\\");
        this.suffix("/");

        if (this.trollian.checked) {
            this.prefix("saccharineMutiny: ");
            if (this.encrypted.checked) {
                this.prefix("[OTP]");
            }
            this.prefix("```\n"); 
            this.suffix("\n```");
        }
    }
}