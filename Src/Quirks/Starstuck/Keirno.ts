import { Quirk } from "../Quirk";
import { CAT_STA } from "../../Category";

export class Keirno extends Quirk {

    constructor() {
        super("Keirno", "??????", CAT_STA, "terezi");
    }

    quirkify(): void {
        if ((<HTMLInputElement>document.getElementById("chkToggleCT")).checked) {
            this.trollifyCustom();
        }
        
        if ((<HTMLInputElement>document.getElementById("chkToggleTC")).checked) {
            this.trollifyCanon();
        }

        this.upperCase();
        this.replaceEmotes(">$1]");
        this.replaceStr("A", "4");
        this.replaceStr("I", "1");
        this.replaceStr("E", "3");
    }
}