import { Quirk } from "../Quirk";
import { CAT_STA } from "../../Category";

export class Sarina extends Quirk {
    trollian: HTMLInputElement;
    encrypted: HTMLInputElement;
    codeblock: HTMLInputElement;

    constructor() {
        super("Sarina", "Rhizen", CAT_STA, "sollux");
        this.trollian = this.addCheckbox("Trollian Chat Handle", "Prefix with trollian chat handle", false);
        this.encrypted = this.addCheckbox("Encrypted Trollian Chat", "Add [OTP] prefix", false);
    }

    quirkify(): void {
        if ((<HTMLInputElement>document.getElementById("chkToggleCT")).checked) {
            this.trollifyCustom();
        }
        
        if ((<HTMLInputElement>document.getElementById("chkToggleTC")).checked) {
            this.trollifyCanon();
        }

        this.upperCase();

        this.replaceStr("HH", "H");
        this.replaceStr("CC", "C");
        this.replaceStr("VV", "V");
        this.replaceStr("[(]", "[");
        this.replaceStr("[)]", "]");
        this.replaceStr("I", "11");
        this.replaceStr("S", "5");
        this.replaceWord("(TOO|TO|TWO)", "2");
        this.replaceStr("T", "7");
        this.replaceStr("G", "6");
        this.replaceStr("E", "3");
        this.replaceStr("H", "|-|");
        this.replaceStr("O", "0");
        this.replaceStr("B", "8");
        this.replaceStr("A", "4");
        this.replaceStr("C", "(");
        this.replaceStr("V", "\\\\/");
        this.replaceStr("55", "22");
        this.replaceStr("77", "7");
        this.replaceStr("66", "6");
        this.replaceStr("33", "3");
        this.replaceStr("00", "0");
        this.replaceStr("88", "8");
        this.replaceStr("44", "4");

        this.quadhornsEmotes();

        if (this.trollian.checked) {
            this.prefix("overclockedTorment: ");
            if (this.encrypted.checked) {
                this.prefix("[OTP]");
            }
            this.prefix("```\n"); 
            this.suffix("\n```");
        }
    }
}