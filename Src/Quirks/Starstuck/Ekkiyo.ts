import { Quirk } from "../Quirk";
import { CAT_STA } from "../../Category";

export class Ekkiyo extends Quirk {

    constructor() {
        super("Ekkiyo", "Xhoveu", CAT_STA, "vriska");
    }

    quirkify(): void {
        if ((<HTMLInputElement>document.getElementById("chkToggleCT")).checked) {
            this.trollifyCustom();
        }
        
        if ((<HTMLInputElement>document.getElementById("chkToggleTC")).checked) {
            this.trollifyCanon();
        }

        this.upperCase();
    }
}