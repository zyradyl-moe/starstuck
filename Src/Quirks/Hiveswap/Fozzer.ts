import { Quirk } from "../Quirk";
import { CAT_HIV } from "../../Category";

export class Fozzer extends Quirk {
    constructor() {
        super("Fozzer", "Velyes", CAT_HIV, "diemen");
    }

    quirkify(): void {
        if ((<HTMLInputElement>document.getElementById("chkToggleCT")).checked) {
            this.trollifyCustom();
        }
        
        if ((<HTMLInputElement>document.getElementById("chkToggleTC")).checked) {
            this.trollifyCanon();
        }
        
        this.replaceStr("\\s", "_");
    }
}