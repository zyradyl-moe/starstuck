import { Quirk } from "../Quirk";
import { CAT_HIV } from "../../Category";

export class Mallek extends Quirk {
    constructor() {
        super("Mallek", "Adalov", CAT_HIV, "vriska");
    }

    quirkify(): void {
        if ((<HTMLInputElement>document.getElementById("chkToggleCT")).checked) {
            this.trollifyCustom();
        }
        
        if ((<HTMLInputElement>document.getElementById("chkToggleTC")).checked) {
            this.trollifyCanon();
        }
        
        this.lowerCase();
        this.replaceStr("\\.", ";");
        this.replaceWord("is not", "!=");
        this.replaceWord("is", "=");
    }
}