import { Quirk } from "../Quirk";
import { CAT_HIV } from "../../Category";

export class Bronya extends Quirk {
    constructor() {
        super("Bronya", "Ursama", CAT_HIV);
    }

    quirkify(): void {
        if ((<HTMLInputElement>document.getElementById("chkToggleCT")).checked) {
            this.trollifyCustom();
        }
        
        if ((<HTMLInputElement>document.getElementById("chkToggleTC")).checked) {
            this.trollifyCanon();
        }
        
        this.prefix("vV ");
        this.suffix(" Vv");
    }
}