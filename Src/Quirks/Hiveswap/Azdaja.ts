import { Quirk } from "../Quirk";
import { CAT_HIV } from "../../Category";

export class Azdaja extends Quirk {
    constructor() {
        super("Azdaja", "Knelax", CAT_HIV, "kuprum");
    }

    quirkify(): void {
        if ((<HTMLInputElement>document.getElementById("chkToggleCT")).checked) {
            this.trollifyCustom();
        }
        
        if ((<HTMLInputElement>document.getElementById("chkToggleTC")).checked) {
            this.trollifyCanon();
        }
        
        this.prefix("||| ");
        this.suffix(" |||");
    }
}