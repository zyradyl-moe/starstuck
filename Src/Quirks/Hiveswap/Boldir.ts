import { Quirk } from "../Quirk";
import { CAT_HIV } from "../../Category";

export class Boldir extends Quirk {
    constructor() {
        super("Boldir", "Lamati", CAT_HIV, "polypa");
    }

    quirkify(): void {
        if ((<HTMLInputElement>document.getElementById("chkToggleCT")).checked) {
            this.trollifyCustom();
        }
        
        if ((<HTMLInputElement>document.getElementById("chkToggleTC")).checked) {
            this.trollifyCanon();
        }
        
        this.lowerCase();
        this.prefix("(");
        this.suffix(")");
    }
}