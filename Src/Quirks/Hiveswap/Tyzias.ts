import { Quirk } from "../Quirk";
import { CAT_HIV } from "../../Category";

export class Tyzias extends Quirk {
    constructor() {
        super("Tyzias", "Entykk", CAT_HIV, "tagora");
    }

    quirkify(): void {
        if ((<HTMLInputElement>document.getElementById("chkToggleCT")).checked) {
            this.trollifyCustom();
        }
        
        if ((<HTMLInputElement>document.getElementById("chkToggleTC")).checked) {
            this.trollifyCanon();
        }
        
        this.lowerCase();
        this.replaceStr("m", "mmmm");
        this.replaceStr("w", "wwww");
    }
}