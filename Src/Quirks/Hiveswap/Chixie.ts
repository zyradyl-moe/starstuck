import { Quirk } from "../Quirk";
import { CAT_HIV } from "../../Category";

export class Chixie extends Quirk {
    constructor() {
        super("Chixie", "Roixmr", CAT_HIV, "skylla");
    }

    quirkify(): void {
        if ((<HTMLInputElement>document.getElementById("chkToggleCT")).checked) {
            this.trollifyCustom();
        }
        
        if ((<HTMLInputElement>document.getElementById("chkToggleTC")).checked) {
            this.trollifyCanon();
        }
        
        this.lowerCase();
        this.replaceStr("\\.", " /");
    }
}