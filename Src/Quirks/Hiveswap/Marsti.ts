import { Quirk } from "../Quirk";
import { CAT_HIV } from "../../Category";

export class Marsti extends Quirk {
    constructor() {
        super("Marsti", "Houtek", CAT_HIV, "diemen");
    }

    quirkify(): void {
        if ((<HTMLInputElement>document.getElementById("chkToggleCT")).checked) {
            this.trollifyCustom();
        }
        
        if ((<HTMLInputElement>document.getElementById("chkToggleTC")).checked) {
            this.trollifyCanon();
        }
        
        this.suffix(" -_-");
    }
}