import { Quirk } from "../Quirk";
import { CAT_HIV } from "../../Category";

export class Vikare extends Quirk {
    constructor() {
        super("Vikare", "Ratite", CAT_HIV, "skylla");
    }

    quirkify(): void {
        if ((<HTMLInputElement>document.getElementById("chkToggleCT")).checked) {
            this.trollifyCustom();
        }
        
        if ((<HTMLInputElement>document.getElementById("chkToggleTC")).checked) {
            this.trollifyCanon();
        }
        
        this.prefix("~");
        this.suffix("~");
    }
}