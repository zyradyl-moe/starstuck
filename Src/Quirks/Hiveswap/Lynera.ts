import { Quirk } from "../Quirk";
import { CAT_HIV } from "../../Category";

export class Lynera extends Quirk {
    constructor() {
        super("Lynera", "Skalbi", CAT_HIV, "bronya");
    }

    quirkify(): void {
        if ((<HTMLInputElement>document.getElementById("chkToggleCT")).checked) {
            this.trollifyCustom();
        }
        
        if ((<HTMLInputElement>document.getElementById("chkToggleTC")).checked) {
            this.trollifyCanon();
        }
        
        this.prefix("-");
    }
}