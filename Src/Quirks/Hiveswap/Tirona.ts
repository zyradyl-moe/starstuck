import { Quirk } from "../Quirk";
import { CAT_HIV } from "../../Category";

export class Tirona extends Quirk {
    constructor() {
        super("Tirona", "Kasund", CAT_HIV, "tagora");
    }

    quirkify(): void {
        if ((<HTMLInputElement>document.getElementById("chkToggleCT")).checked) {
            this.trollifyCustom();
        }
        
        if ((<HTMLInputElement>document.getElementById("chkToggleTC")).checked) {
            this.trollifyCanon();
        }
        
        this.lowerCase();
        this.replaceStr("e", "33");
    }
}