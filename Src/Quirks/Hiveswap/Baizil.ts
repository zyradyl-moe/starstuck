import { Quirk } from "../Quirk";
import { CAT_HIV } from "../../Category";

export class Baisil extends Quirk {
    constructor() {
        super("Baisil", "Soleil", CAT_HIV, "chahut");
    }

    quirkify(): void {
        if ((<HTMLInputElement>document.getElementById("chkToggleCT")).checked) {
            this.trollifyCustom();
        }
        
        if ((<HTMLInputElement>document.getElementById("chkToggleTC")).checked) {
            this.trollifyCanon();
        }
        
        this.lowerCase();
    }
}