import { Quirk } from "../Quirk";
import { CAT_HIV } from "../../Category";

export class Ardata extends Quirk {
    constructor() {
        super("Ardata", "Carmia", CAT_HIV, "vriska");
    }

    quirkify(): void {
        if ((<HTMLInputElement>document.getElementById("chkToggleCT")).checked) {
            this.trollifyCustom();
        }
        
        if ((<HTMLInputElement>document.getElementById("chkToggleTC")).checked) {
            this.trollifyCanon();
        }
        
        this.lowerCase();
        this.replaceStr("i", "iii");
    }
}