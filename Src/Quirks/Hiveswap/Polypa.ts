import { Quirk } from "../Quirk";
import { CAT_HIV } from "../../Category";

export class Polypa extends Quirk {
    constructor() {
        super("Polypa", "Goezee", CAT_HIV);
    }

    quirkify(): void {
        if ((<HTMLInputElement>document.getElementById("chkToggleCT")).checked) {
            this.trollifyCustom();
        }
        
        if ((<HTMLInputElement>document.getElementById("chkToggleTC")).checked) {
            this.trollifyCanon();
        }
        
        this.lowerCase();
        this.replaceStr("\\.", " *");
        this.suffix(" *|");
    }
}