import { Quirk } from "../Quirk";
import { CAT_HIV } from "../../Category";

export class Diemen extends Quirk {
    constructor() {
        super("Diemen", "Xicali", CAT_HIV);
    }

    quirkify(): void {
        if ((<HTMLInputElement>document.getElementById("chkToggleCT")).checked) {
            this.trollifyCustom();
        }
        
        if ((<HTMLInputElement>document.getElementById("chkToggleTC")).checked) {
            this.trollifyCanon();
        }
        
        this.lowerCase();
        this.prefix("(| ");
        this.suffix(" |)");
    }
}