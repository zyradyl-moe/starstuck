import { Quirk } from "../Quirk";
import { CAT_HIV } from "../../Category";

export class Karako extends Quirk {
    constructor() {
        super("Karako", "Pierot", CAT_HIV, "chahut");
    }

    quirkify(): void {
        if ((<HTMLInputElement>document.getElementById("chkToggleCT")).checked) {
            this.trollifyCustom();
        }
        
        if ((<HTMLInputElement>document.getElementById("chkToggleTC")).checked) {
            this.trollifyCanon();
        }
        
        this.replaceStr("\\w+", "honk", true);
    }
}