import { Quirk } from "../Quirk";
import { CAT_HIV } from "../../Category";

export class Tagora extends Quirk {
    constructor() {
        super("Tagora", "Gorjek", CAT_HIV);
    }

    quirkify(): void {
        if ((<HTMLInputElement>document.getElementById("chkToggleCT")).checked) {
            this.trollifyCustom();
        }
        
        if ((<HTMLInputElement>document.getElementById("chkToggleTC")).checked) {
            this.trollifyCanon();
        }
        
        this.suffix(" *_________");
    }
}