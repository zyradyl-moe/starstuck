import { Quirk } from "../Quirk";
import { CAT_HIV } from "../../Category";

export class Tegiri extends Quirk {
    constructor() {
        super("Tegiri", "Kalbur", CAT_HIV, "tagora");
    }

    quirkify(): void {
        if ((<HTMLInputElement>document.getElementById("chkToggleCT")).checked) {
            this.trollifyCustom();
        }
        
        if ((<HTMLInputElement>document.getElementById("chkToggleTC")).checked) {
            this.trollifyCanon();
        }
        
        this.replaceStr("[Ll]", "/");
    }
}