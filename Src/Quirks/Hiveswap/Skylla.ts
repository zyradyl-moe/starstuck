import { Quirk } from "../Quirk";
import { CAT_HIV } from "../../Category";

export class Skylla extends Quirk {
    constructor() {
        super("Skylla", "Koriga", CAT_HIV);
    }

    quirkify(): void {
        if ((<HTMLInputElement>document.getElementById("chkToggleCT")).checked) {
            this.trollifyCustom();
        }
        
        if ((<HTMLInputElement>document.getElementById("chkToggleTC")).checked) {
            this.trollifyCanon();
        }
        
        this.replaceStr("y", "yy");
        this.replaceStr("Y", "YY");
    }
}