import { Quirk } from "../Quirk";
import { CAT_HIV } from "../../Category";

export class Kuprum extends Quirk {
    constructor() {
        super("Kuprum", "Maxlol", CAT_HIV);
    }

    quirkify(): void {
        if ((<HTMLInputElement>document.getElementById("chkToggleCT")).checked) {
            this.trollifyCustom();
        }
        
        if ((<HTMLInputElement>document.getElementById("chkToggleTC")).checked) {
            this.trollifyCanon();
        }
        
        this.lowerCase();
        this.changeCase("\\bl+o[ol]*l\\b", true);
        this.prefix(">");
    }
}