import { Quirk } from "../Quirk";
import { CAT_HIV } from "../../Category";

export class Trizza extends Quirk {
    constructor() {
        super("Trizza", "Tethis", CAT_HIV);
    }

    quirkify(): void {
        if ((<HTMLInputElement>document.getElementById("chkToggleCT")).checked) {
            this.trollifyCustom();
        }
        
        if ((<HTMLInputElement>document.getElementById("chkToggleTC")).checked) {
            this.trollifyCanon();
        }
        
        this.replaceStr("[Ww]", "ψ");
    }
}