import { Quirk } from "../Quirk";
import { CAT_HIV } from "../../Category";

export class Chahut extends Quirk {
    constructor() {
        super("Chahut", "Maenad", CAT_HIV);
    }

    quirkify(): void {
        if ((<HTMLInputElement>document.getElementById("chkToggleCT")).checked) {
            this.trollifyCustom();
        }
        
        if ((<HTMLInputElement>document.getElementById("chkToggleTC")).checked) {
            this.trollifyCanon();
        }
        
        this.lowerCase();
        this.replaceStr("t", "T");
    }
}