import { Quirk } from "../Quirk";
import { CAT_HIV } from "../../Category";

export class Zebede extends Quirk {
    constructor() {
        super("Zebede", "Tongva", CAT_HIV, "kuprum");
    }

    quirkify(): void {
        if ((<HTMLInputElement>document.getElementById("chkToggleCT")).checked) {
            this.trollifyCustom();
        }
        
        if ((<HTMLInputElement>document.getElementById("chkToggleTC")).checked) {
            this.trollifyCanon();
        }
        
        this.lowerCase();
        this.replaceStr("s\\b", "z");
        this.replaceEmotes("z$1$2");
    }
}