import { Quirk } from "../Quirk";
import { CAT_HIV } from "../../Category";

export class Remele extends Quirk {
    constructor() {
        super("Remele", "Namaaq", CAT_HIV, "vriska");
    }

    quirkify(): void {
        if ((<HTMLInputElement>document.getElementById("chkToggleCT")).checked) {
            this.trollifyCustom();
        }
        
        if ((<HTMLInputElement>document.getElementById("chkToggleTC")).checked) {
            this.trollifyCanon();
        }
        
        this.randReplace("(\\w+)", "$1e", 0.5);
    }
}