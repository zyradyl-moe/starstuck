import { Quirk } from "../Quirk";

export class Karkat extends Quirk {
    constructor() {
        super("Karkat", "Vantas");
    }

    quirkify(): void {
        if ((<HTMLInputElement>document.getElementById("chkToggleCT")).checked) {
            this.trollifyCustom();
        }
        
        if ((<HTMLInputElement>document.getElementById("chkToggleTC")).checked) {
            this.trollifyCanon();
        }
        
        this.upperCase();
    }
}