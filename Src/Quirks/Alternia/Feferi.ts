import { Quirk } from "../Quirk";

export class Feferi extends Quirk {
    puns: HTMLInputElement;

    constructor() {
        super("Feferi", "Peixes");
        this.puns = this.addCheckbox("Fish Puns", "Shellf-explanatory!", true)
    }

    quirkify(): void {
        if ((<HTMLInputElement>document.getElementById("chkToggleCT")).checked) {
            this.trollifyCustom();
        }
        
        if ((<HTMLInputElement>document.getElementById("chkToggleTC")).checked) {
            this.trollifyCanon();
        }
        
        if (this.puns.checked) {
            this.fishPuns();
        }

        this.replaceStr("[Hh]", ")(");
        this.replaceStr("E", "-E");
        this.tiaraEmotes();
    }
}