import { Quirk } from "../Quirk";

export class Aradia extends Quirk {
    dead: HTMLInputElement;

    constructor() {
        super("Aradia", "Medigo");
        this.dead = this.addCheckbox("Dead Quirk", "Aradia's typing quirk used when she is dead (o --> 0).", true);
    }

    quirkify(): void {
        if ((<HTMLInputElement>document.getElementById("chkToggleCT")).checked) {
            this.trollifyCustom();
        }
        
        if ((<HTMLInputElement>document.getElementById("chkToggleTC")).checked) {
            this.trollifyCanon();
        }

        this.lowerCase();
        
        if (this.dead.checked) {
            this.replaceStr("o", "0");

            if (Math.random() <= 0.1) {
                this.suffix(" ribbit");
            }
        }
    }
}