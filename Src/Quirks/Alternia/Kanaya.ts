import { Quirk } from "../Quirk";

export class Kanaya extends Quirk {
    constructor() {
        super("Kanaya", "Maryam");
    }

    quirkify(): void {
        if ((<HTMLInputElement>document.getElementById("chkToggleCT")).checked) {
            this.trollifyCustom();
        }
        
        if ((<HTMLInputElement>document.getElementById("chkToggleTC")).checked) {
            this.trollifyCanon();
        }
        this.replaceStr("[.!?\\-\'\"/\,]", "");
        this.titleCase();
    }
}