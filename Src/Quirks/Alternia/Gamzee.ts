import { Quirk } from "../Quirk";

export class Gamzee extends Quirk {
    constructor() {
        super("Gamzee", "Makara");
    }

    quirkify(): void {
        if ((<HTMLInputElement>document.getElementById("chkToggleCT")).checked) {
            this.trollifyCustom();
        }
        
        if ((<HTMLInputElement>document.getElementById("chkToggleTC")).checked) {
            this.trollifyCanon();
        }
        
        this.alternatingCaps();
        this.replaceEmotes("$1o$2");
    }
}