import { Quirk } from "../Quirk";

export class Eridan extends Quirk {
    constructor() {
        super("Eridan", "Ampora");
    }

    quirkify(): void {
        if ((<HTMLInputElement>document.getElementById("chkToggleCT")).checked) {
            this.trollifyCustom();
        }
        
        if ((<HTMLInputElement>document.getElementById("chkToggleTC")).checked) {
            this.trollifyCanon();
        }
        
        this.lowerCase();
        this.replaceStr("w", "ww");
        this.replaceStr("v", "vv");
    }
}