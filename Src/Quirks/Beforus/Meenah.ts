import { Quirk } from "../Quirk";
import { CAT_BEF } from "../../Category";

export class Meenah extends Quirk {
    puns: HTMLInputElement;

    constructor() {
        super("Meenah", "Peixes", CAT_BEF, "feferi");
        this.puns = this.addCheckbox("Fish Puns", "Shellf-explanatory!", true)
    }

    quirkify(): void {
        if ((<HTMLInputElement>document.getElementById("chkToggleCT")).checked) {
            this.trollifyCustom();
        }
        
        if ((<HTMLInputElement>document.getElementById("chkToggleTC")).checked) {
            this.trollifyCanon();
        }
        
        if (this.puns.checked) {
            this.fishPuns();
        }
        
        this.replaceStr("H", ")(");
        this.replaceStr("E", "-E");
        this.tiaraEmotes();
    }
}