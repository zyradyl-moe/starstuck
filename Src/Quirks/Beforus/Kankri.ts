import { Quirk } from "../Quirk";
import { CAT_BEF } from "../../Category";

export class Kankri extends Quirk {
    constructor() {
        super("Kankri", "Vantas", CAT_BEF);
    }

    quirkify(): void {
        if ((<HTMLInputElement>document.getElementById("chkToggleCT")).checked) {
            this.trollifyCustom();
        }
        
        if ((<HTMLInputElement>document.getElementById("chkToggleTC")).checked) {
            this.trollifyCanon();
        }
        
        this.replaceStr("[Bb]", "6");
        this.replaceStr("[Oo]", "9");
    }
}