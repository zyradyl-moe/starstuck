import { Quirk } from "../Quirk";
import { CAT_BEF } from "../../Category";

export class Kurloz extends Quirk {
    constructor() {
        super("Kurloz", "Makara", CAT_BEF, "gamzee");
    }

    quirkify(): void {
        if ((<HTMLInputElement>document.getElementById("chkToggleCT")).checked) {
            this.trollifyCustom();
        }
        
        if ((<HTMLInputElement>document.getElementById("chkToggleTC")).checked) {
            this.trollifyCanon();
        }
        
        this.upperCase();
    }
}