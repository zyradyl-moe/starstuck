import { Quirk } from "../Quirk";
import { CAT_BEF } from "../../Category";

export class Latula extends Quirk {
    constructor() {
        super("Latula", "Pyrope", CAT_BEF, "terezi");
    }

    quirkify(): void {
        if ((<HTMLInputElement>document.getElementById("chkToggleCT")).checked) {
            this.trollifyCustom();
        }
        
        if ((<HTMLInputElement>document.getElementById("chkToggleTC")).checked) {
            this.trollifyCanon();
        }
        
        this.lowerCase();
        this.replaceStr("a", "4");
        this.replaceStr("i", "1");
        this.replaceStr("e", "3");
    }
}